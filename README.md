# Multi-Target-Tracking
Multi Target Tracking simulating radar.
Used EKF, IPDA

- Result

![](result/10runs.JPG)

- RMSE

![](result/tr10runs.JPG)
![](result/xy10runs.JPG)
